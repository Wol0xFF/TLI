<?php
/* Smarty version 3.1.30, created on 2018-05-24 09:25:45
  from "C:\wamp\www\tli\www\templates\pathologies.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b0685196e3467_45086195',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '445410db181c254b6600e5ddf921e873255d0df1' => 
    array (
      0 => 'C:\\wamp\\www\\tli\\www\\templates\\pathologies.tpl',
      1 => 1527153945,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout.tpl' => 1,
  ),
),false)) {
function content_5b0685196e3467_45086195 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_52755b068519693415_13059752', 'pageTitle');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_123445b0685196b2f19_12286506', 'mainAside');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_184495b0685196e0297_48143896', 'body');
?>

<?php $_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:layout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'pageTitle'} */
class Block_52755b068519693415_13059752 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Pathologies | Site Acupuncture<?php
}
}
/* {/block 'pageTitle'} */
/* {block 'mainAside'} */
class Block_123445b0685196b2f19_12286506 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <p>Bonjour, <strong><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</strong>. <a href="logout.php">Déconnexion</a></p>
<?php
}
}
/* {/block 'mainAside'} */
/* {block 'body'} */
class Block_184495b0685196e0297_48143896 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <main>
        <h2>Catalogue des pathologies</h2>

        <div class="pathology pathologyHeader">
            <span class="pathologyDescription">Description</span>
            <span class="pathologyMeridian">Meridien</span>
            <span class="pathologyType">Type</span>
        </div>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pathologies']->value, 'pathology');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['pathology']->value) {
?>
        <div class="pathology">
            <span class="pathologyDescription"><?php echo $_smarty_tpl->tpl_vars['pathology']->value->getDescription();?>
</span>
            <span class="pathologyMeridian"><?php echo $_smarty_tpl->tpl_vars['pathology']->value->getMeridian()->getName();?>
</span>
            <span class="pathologyType"><?php echo $_smarty_tpl->tpl_vars['pathology']->value->getType();?>
</span>
        </div>
        <?php
}
} else {
?>

        <p>Il n'y a pas de pathologies à afficher.</p>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </main>
<?php
}
}
/* {/block 'body'} */
}
