<?php
date_default_timezone_set('Europe/Paris');
define('ROOT_DIR', '/var/www/html');
define('SMARTY_DIR', ROOT_DIR . '/lib/Smarty-3.1.30/libs/');

require_once(SMARTY_DIR . 'Smarty.class.php');
require_once('controller/AuthenticationController.php');
require_once('controller/PathologyController.php');
require_once('service/PathologyService.php');

class Webapp {
    private $smarty;
    private $pdo;
    private $authenticationController;
    private $pathologyController;

    public $config;

    public function __construct() {
        $this->initConfig();
        $this->initDb();
        $this->initSmarty();
        $this->initControllers();
    }

    private function initConfig() {
        $this->config['root_url'] = 'http://localhost';
        $this->config['paths'] = Array(
            'home' => '/index.php'
        );

        $this->config['database'] = Array(
            'host' => 'mysql',
            'dbname' => 'tli',
            'username' => 'tli',
            'password' => 'tli',
            'options' => Array(
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            )
        );
    }

    private function initDb() {
        $host = $this->config['database']['host'];
        $dbname = $this->config['database']['dbname'];
        $username = $this->config['database']['username'];
        $password = $this->config['database']['password'];
        $options = $this->config['database']['options'];
        $this->pdo = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password, $options);
    }

    private function initSmarty() {
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir(ROOT_DIR . '/templates');
        $this->smarty->setCompileDir(ROOT_DIR . '/templates_c');
        $this->smarty->setConfigDir(ROOT_DIR . '/configs');
        $this->smarty->setCacheDir(ROOT_DIR . '/cache');
    }

    private function initControllers() {
        $this->authenticationController = new AuthenticationController($this);
        $this->pathologyController = new PathologyController($this, new PathologyService($this));
    }

    public function getSmarty() {
        return $this->smarty;
    }

    public function getPdo() {
        return $this->pdo;
    }

    public function getAuthenticationController() {
        return $this->authenticationController;
    }

    public function getPathologyController() {
        return $this->pathologyController;
    }
}
