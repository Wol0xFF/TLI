<?php
require_once('Webapp.php');
session_start();

$webapp = new Webapp();
$config = $webapp->config;
$authenticationController = $webapp->getAuthenticationController();

$authenticationController->disconnect();

header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?disconnectOk');
die();
