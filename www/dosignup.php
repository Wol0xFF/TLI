<?php require_once('Webapp.php'); session_start();
$webapp = new Webapp();
$config = $webapp->config;
$authenticationController = $webapp->getAuthenticationController();

$sanitizedUsername = filter_var($_POST['signupFormUsername'], FILTER_SANITIZE_STRING);
$sanitizedPassword = filter_var($_POST['signupFormPassword'], FILTER_SANITIZE_STRING);
$sanitizedPasswordConfirmation = filter_var($_POST['signupFormPasswordConfirmation'], FILTER_SANITIZE_STRING);

switch($authenticationController->register($sanitizedUsername, $sanitizedPassword, $sanitizedPasswordConfirmation)) {
    case AuthenticationController::ALREADY_AUTHENTICATED:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?signupError=alreadyAuthenticated');
        break;
    case AuthenticationController::INVALID_FORMAT:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?signupError=invalidFormat');
        break;
    case AuthenticationController::USERNAME_ALREADY_REGISTERED:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?signupError=usernameAlreadyRegistered');
        break;
    case AuthenticationController::PASSWORDS_DO_NOT_MATCH:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?signupError=passwordsDoNotMatch');
        break;
    case AuthenticationController::REGISTER_OK:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?signupOk');
        break;
    default:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?signupError=unknownError');
        break;
}

echo 'Redirection en cours... <a href="index.php">Cliquez ici si la page ne se charge pas.</a>';
die();