<?php
require_once('Webapp.php');
session_start();

$webapp = new Webapp();
$config = $webapp->config;
$authenticationController = $webapp->getAuthenticationController();
if (!$authenticationController->isAuthenticated()) {
    header('Location: ' . $config['root_url'] . $config['paths']['home']);
    die();
}

$pathologies = $webapp->getPathologyController()->findAll();

$smarty = $webapp->getSmarty();
$smarty->assign('username', $authenticationController->getAuthenticatedUser()->getUsername());
$smarty->assign('pathologies', $pathologies);
$smarty->display('pathologies.tpl');