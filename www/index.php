<?php
require_once('Webapp.php');
session_start();

$webapp = new Webapp();
$authenticationController = $webapp->getAuthenticationController();
$hasLoginError = isset($_GET['loginError']);
$loginErrorMessage = "";
if ($hasLoginError) {
    $loginError = filter_var($_GET['loginError'], FILTER_SANITIZE_STRING);
    switch ($loginError) {
        case "invalidCredentials":
            $loginErrorMessage = "Le nom d'utilisateur ou le mot de passe est incorrect. Veuillez réessayer.";
            break;
        case "alreadyAuthenticated":
            $loginErrorMessage = 'Vous êtes déjà connecté. <a href="logout.php">Déconnectez-vous</a> ou <a href="index.php">retournez à l\'accueil.</a>';
            break;
        default:
            $loginErrorMessage = "Erreur inconnue. Veuillez réessayer dans un moment.";
            break;
    }
}

$hasSignupError = isset($_GET['signupError']);
$signupErrorMessage = "";
if ($hasSignupError) {
    $signupError = filter_var($_GET['signupError'], FILTER_SANITIZE_STRING);
    switch ($signupError) {
        case "alreadyAuthenticated":
            $signupErrorMessage = 'Vous êtes déjà connecté. <a href="logout.php">Déconnectez-vous</a> ou <a href="index.php">retournez à l\'accueil.</a>';
            break;
        case "invalidFormat":
            $signupErrorMessage = "Le format des champs est invalide.";
            break;
        case "usernameAlreadyRegistered":
            $signupErrorMessage = "Le nom d'utilisateur que vous avez entré est déjà associé à un utilisateur. Veuillez en choisir un autre.";
            break;
        case "passwordsDoNotMatch":
            $signupErrorMessage = "Les mots de passes entrés ne correspondent pas. Veuillez réessayer.";
            break;
        default:
            $signupErrorMessage = "Erreur inconnue. Veuillez réessayer dans un moment.";
    }
}
$smarty = $webapp->getSmarty();
$smarty->assign('hasLoginError', $hasLoginError);
$smarty->assign('loginOk', isset($_GET['loginOk']));
$smarty->assign('loginErrorMessage', $loginErrorMessage);

$smarty->assign('hasSignupError', $hasSignupError);
$smarty->assign('signupOk', isset($_GET['signupOk']));
$smarty->assign('signupErrorMessage', $signupErrorMessage);

$smarty->assign('disconnectOk', isset($_GET['disconnectOk']));
$smarty->assign('authenticated', $authenticationController->isAuthenticated());
if ($authenticationController->isAuthenticated()) {
    $smarty->assign('username', $authenticationController->getAuthenticatedUser()->getUsername());
}
$smarty->display('index.tpl');
