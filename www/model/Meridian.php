<?php
class Meridian {
    private $code;
    private $name;
    private $element;
    private $yin;

    public function __construct($code, $name, $element, $yin) {
        $this->code = $code;
        $this->name = $name;
        $this->element = $element;
        $this->yin = $yin;
    }

    public function getCode() {
        return $this->code;
    }

    public function getName() {
        return $this->name;
    }

    public function getElement() {
        return $this->element;
    }

    public function getYin() {
        return $this->yin;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setElement($element) {
        $this->element = $element;
    }

    public function setYin($yin) {
        $this->yin = $yin;
    }
}