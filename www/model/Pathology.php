<?php
class Pathology {
    private $id;
    private $meridian;
    private $type;
    private $description;
    private $symptoms;

    public function __construct($id, $meridian, $type, $description, $symptoms) {
        $this->id = $id;
        $this->meridian = $meridian;
        $this->type = $type;
        $this->description = $description;
        $this->symptoms = $symptoms;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getMeridian() {
        return $this->meridian;
    }

    public function setMeridian($meridian) {
        $this->meridian = $meridian;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getSymptoms() {
        return $this->symptoms;
    }

    public function setSymptoms($symptoms) {
        $this->symptoms = $symptoms;
    }
}