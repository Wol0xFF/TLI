<?php

require_once('model/Pathology.php');
require_once('model/Meridian.php');

class PathologyService {
    private $webapp;

    public function __construct($webapp) {
        $this->webapp = $webapp;
    }

    public function findAll() {
        $pdo = $this->webapp->getPdo();

        $findAllQuery = " 
        SELECT * FROM patho p
        LEFT JOIN meridien m ON p.mer = m.code 
        /*LEFT JOIN symptome ON (symptome.idS, patho.idP) IN (SELECT idS, idP FROM symptpatho)*/
        ";

        $statement = $pdo->prepare($findAllQuery);
        $statement->execute();
        $pathologies = $statement->fetchAll(PDO::FETCH_ASSOC);
        $pathologyObjects = Array();
        foreach($pathologies as $pathology) {
            $pathologyObject = new Pathology(
                $pathology["idP"],
                new Meridian(
                    $pathology["code"],
                    $pathology["nom"],
                    $pathology["element"],
                    $pathology["yin"]
                ),
                $pathology["type"],
                $pathology["desc"],
                Array()
            );
            array_push($pathologyObjects, $pathologyObject);
        }
        return $pathologyObjects;
    }

}