USE tli;
CREATE TABLE member (
  id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(30),
  password VARCHAR(60) /* bcrypt */
);

INSERT INTO member (username, password) VALUES ('test', '$2y$10$sOVRLouWu7Ez1NnHBYS5W.kQTAv6GNF2d3oLsP6izguqKaEW493Ui') /* test */
INSERT INTO member (username, password) VALUES ('test2', '$2y$10$/r7nX0c5Rd8v50K1t.Sbd.4p24n9nhIH.PZwcX3pwMrNNJFXOLPkS') /* test */
